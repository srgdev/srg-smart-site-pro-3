<?php
/**
 * The main template file.
 *
 */

get_header(); ?>

<div id="bannerRow" class="row subBanner bgcolor-primary" style="background-image:url(<?php echo get_theme_mod('srg_theme_banner_image_top') ? get_theme_mod('srg_theme_banner_image_top') : get_template_directory_uri().'/images/bg-bannerrow.jpg';?>)">
	<div class="rowInner">
    	
    	<h1><?php echo get_the_title(get_option('page_for_posts')); ?></h1>
	
    </div> <!-- End rowInner -->
</div> <!-- End row -->


<div id="contentRow" class="row">
	<div class="rowInner <?php echo is_active_sidebar('blog-sidebar') ? 'hasSidebar' : ''; ?>">
    
    		<?php if(is_active_sidebar('blog-sidebar')): ?>
            	
                	<?php get_sidebar('blog'); ?>
                
            <?php endif; ?>
        
        	<?php get_template_part('loop', 'index'); ?>
    	
  	</div> <!-- End rowInner -->
</div> <!-- End row -->

<?php get_footer(); ?>