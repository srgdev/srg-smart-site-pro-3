<?php
/**
* Template part for posts
*
*/
?>

<!-- BEGIN POST -->
    <div class="post">
        <div class="postHeader">
        <?php if(has_post_thumbnail()): ?>
            <div class="postImage"><?php the_post_thumbnail('newsfeed'); ?></div>
            <?php endif; ?>
            <div class="postDate bgcolor-secondary">
			
            	<?php if(get_post_type() == 'event'): ?>
        
        			<?php echo date('M d', strtotime(get_field('start_date'))); ?>
                
                <?php else: ?>
            
					<?php echo strtoupper(get_the_time('M d')); ?>
            
            	<?php endif; ?>
            
            </div>
        </div>
        <div class="postContent">
            <?php the_content(); ?>
        </div>
    </div>
    <!-- END POST -->