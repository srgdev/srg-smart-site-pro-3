<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

<div id="bannerRow" class="row subBanner bgcolor-primary" style="background-image:url(<?php echo get_theme_mod('srg_theme_banner_image_top') ? get_theme_mod('srg_theme_banner_image_top') : get_template_directory_uri().'/images/bg-bannerrow.jpg';?>)">
	<div class="rowInner">
    	
    	<h1><?php _e('404 Page Not Found', 'srg'); ?></h1>
	
    </div> <!-- End rowInner -->
</div> <!-- End row -->

<div id="contentRow" class="row">
	<div class="rowInner <?php echo is_active_sidebar('blog-sidebar') ? 'hasSidebar' : ''; ?>">
    
    		<?php if(is_active_sidebar('blog-sidebar')): ?>
            	
                	<?php get_sidebar('blog'); ?>
                
            <?php endif; ?>
        
        	<div id="content">
            
            <p><?php _e( 'Apologies, but the page you requested could not be found. Perhaps searching will help.', 'srg' ); ?></p>
				<?php get_search_form(); ?>
                
            </div>
            
            <script type="text/javascript">
		// focus on search field after it has loaded
		document.getElementById('s') && document.getElementById('s').focus();
	</script>
    	
  	</div> <!-- End rowInner -->
</div> <!-- End row -->

<?php get_footer(); ?>