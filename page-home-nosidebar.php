<?php
/**
 * Template Name: Home page, widgetized content
 *
 */

get_header(); ?>

	<?php if(get_theme_mod('srg_theme_showslider') == '1'): ?>
	    <div id="bannerRow" class="row bgcolor-primary" style="background-image:url(<?php echo get_theme_mod('srg_theme_banner_image_top') ? get_theme_mod('srg_theme_banner_image_top') : get_template_directory_uri().'/images/bg-bannerrow.jpg';?>)">
            <div class="rowInner">
              <div id="pagerBar"><div id="pager"></div></div> 
                <div id="slider" class="cycle-slideshow" data-cycle-fx="scrollHorz" data-cycle-pager="#pager" data-cycle-pager-template="<a href=#></a>" data-cycle-timeout="4000" data-cycle-slides=".slide" data-cycle-pause-on-hover="true">    
                <?php $args = array('posts_per_page' => get_theme_mod('srg_theme_numslides'), 'post_type' => 'slide'); ?>
                <?php $slides = new WP_Query($args); ?>
                <?php if($slides->have_posts()): while ( $slides->have_posts() ) : $slides->the_post(); ?>
                     <div class="slide">
                        <div class="slideContent">
                            <h1><?php the_title(); ?></h1>
                            <?php the_excerpt(); ?>
                        </div>
                        <div class="slideImage bgcolor-body"><?php the_post_thumbnail('slider'); ?></div>
                        <br class="clear">
                    </div>
                <?php endwhile; // End slides loop. ?>
                <?php else: ?>
                <div class="noPosts">
                    <h1 class="noPostsMessage">Whoops!  You haven't added any slides yet.</h1>
                </div>
                <?php endif; ?>
                </div> <!-- End slider -->
            </div> <!-- End rowInner -->
        </div> <!-- End row -->
    <?php endif; ?>

    <?php if(get_theme_mod('srg_theme_showWidgets') == '1'): ?>
    	<?php if(is_active_sidebar('home-box')): ?>
            <div id="actionRow" class="row">
                <div class="rowInner">
    
    				<?php if ( ! dynamic_sidebar( 'home-box' ) ) : ?>
    				<?php endif; // end home page widget box area ?>
                    
                    <br class="clear">
                    
                </div> <!-- End rowInner -->
            </div> <!-- End row -->
        <?php endif; ?>
    <?php endif; ?>
    
    <?php if(get_theme_mod('srg_theme_showHomeNews') == '1'): ?>
    	<?php 
    	$posts_per_page = 3*get_theme_mod('srg_theme_showNumPosts');
        $args = array('post_type' => 'post', 'posts_per_page' => $posts_per_page); 
        $news = new WP_Query($args);
        ?>
        <?php if(is_front_page()): ?>
            <div id="contentRow" class="row">
                <div class="rowInner">
                    <div id="content" class="homeNews">
                        <h2><?php echo get_theme_mod('srg_theme_homeNewsTitle'); ?></h2>
                        <?php $i = 0; ?>
                        <?php if($news->have_posts()): while($news->have_posts()): $news->the_post(); ?>
                            <?php if(($i % 3 == 0) && $i != 0):?>
                            <div class="newsWidgetBreaker"><br class="clear"/><br /><hr /><br /><br /></div>
                            <?php endif; ?>
                            <?php $i++; ?>
                            <?php get_template_part('article', 'excerpt'); ?>
                        <?php endwhile; ?>
                        <?php else: ?>
                            <!--BEGIN NOPOSTS MESSAGE-->
                            <div class="noPosts">
                                <h1 class="noPostsMessage">Whoops!  You haven't added any posts yet!  Go ahead and add some.</h1>
                            </div>
                            <!--END MESSAGE-->
                        <?php endif; ?>
                        <br class="clear">
                    </div> <!-- End content -->
                    <div id="loadMore"><a href="<?php echo get_permalink(get_option('page_for_posts')); ?>">see all news <i class="fa fa-caret-square-o-right"></i></a></div>
                </div> <!-- End rowInner -->
            </div> <!-- End row -->
        <?php endif; ?>
    <?php endif; ?>

<?php get_footer(); ?>