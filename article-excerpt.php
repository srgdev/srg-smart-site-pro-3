<?php
/**
* Template part for posts
*
*/
?>

<!-- BEGIN POST -->
    <div class="post">
        <div class="postHeader">
        <?php if(has_post_thumbnail()): ?>
            <div class="postImage"><?php the_post_thumbnail('newsfeed'); ?></div>
            <?php endif; ?>
            <div class="postDate bgcolor-secondary">
			
            	<?php if(get_post_type() == 'event'): ?>
        
        			<?php echo date('M d', strtotime(get_field('start_date'))); ?>
                
                <?php else: ?>
            
					<?php echo strtoupper(get_the_time('M d')); ?>
            
            	<?php endif; ?>
            
            </div>
        </div>
        <div class="postContent">
            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
            
            <?php if(get_post_type() == 'event'): ?>
            
                <h4> Event Date: <?php echo date('F j, Y', strtotime(get_field('start_date'))); ?>
					<?php echo get_field('end_date') && get_field('end_date') != get_field('start_date') ? ' - '.date('F j, Y', strtotime(get_field('end_date'))) : ''; ?> ,
					<?php echo get_field('times') ? get_field('times') : ''; ?>
                    </h4>
                
            <?php endif; ?>
            
            <?php the_excerpt(); ?>
        </div>
    </div>
    <!-- END POST -->