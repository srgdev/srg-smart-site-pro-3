<?php
/**
 * The template for displaying the footer.
 *
 */
?>
<div id="footerRow" class="row bgcolor-body">
	<div class="rowInner">
	
    	<div id="footLogo">
        	<a href="<?php bloginfo('url'); ?>"><img src="<?php echo get_theme_mod('srg_theme_logo_footer') ? get_theme_mod('srg_theme_logo_footer') : get_template_directory_uri().'/images/logo-footer.png'; ?>"></a>
        </div>
        
        <div id="footRight">
        	
            <?php srg_nav_menu('footer'); ?>
            
            <br class="clear">
            
			<div id="footerContent">
            	
                <?php srg_footer_text(); ?>
                
          	</div>
        
        </div>
    	
        <br class="clear">
        
  	</div> <!-- End rowInner -->
</div> <!-- End row -->
<?php wp_footer(); ?>
</body>
</html>