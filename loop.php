<?php
/**
 * The loop that displays regular posts.
 *
 */
?>

<div id="content">
    
	<?php if (have_posts() ): while( have_posts() ): the_post(); ?>
    
        <?php get_template_part('article', 'excerpt'); ?>
    
    <?php endwhile; else: ?>
    
    <div class="noPosts">
                	<h1 class="noPostsMessage">Whoops!  You haven't added any posts yet.</h1>
                </div>
    
    <?php endif; ?>
    
    <br class="clear">
            
</div> <!-- End content -->

<?php if (  $wp_query->max_num_pages > 1 ) : ?>
    <div id="loadMore">
	<?php if($wp_query->get('paged') || $wp_query->get('paged') > 1): ?>
    <a class="loadPrev" href="<?php previous_posts(); ?>"><i class="fa fa-caret-square-o-left"></i> prev</a>
    <?php endif; ?>
    
    <?php if ($next_url = next_posts($wp_query->max_num_pages, false)): ?>
    <a class="loadNext" href="<?php echo $next_url; ?>">next <i class="fa fa-caret-square-o-right"></i></a>
    <?php endif;?>
    
    <br class="clear">
</div>
<?php endif; ?>
