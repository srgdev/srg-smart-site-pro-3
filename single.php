<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

<div id="bannerRow" class="row subBanner bgcolor-primary" style="background-image:url(<?php echo get_theme_mod('srg_theme_banner_image_top') ? get_theme_mod('srg_theme_banner_image_top') : get_template_directory_uri().'/images/bg-bannerrow.jpg';?>)">
	<div class="rowInner">
    	
    	<h1><?php echo get_the_title(get_option('page_for_posts')); ?></h1>
	
    </div> <!-- End rowInner -->
</div> <!-- End row -->

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <div id="contentRow" class="row">
        <div class="rowInner <?php echo is_active_sidebar('blog-sidebar') ? 'hasSidebar' : ''; ?>">
        
                <?php if(is_active_sidebar('blog-sidebar')): ?>
                    
                        <?php get_sidebar('blog'); ?>
                    
                <?php endif; ?>
                
                <div id="content">
                
                	<h2><?php the_title(); ?></h2>
            
	                <?php get_template_part('article', 'content'); ?>
                
                </div>
            
        </div> <!-- End rowInner -->
    </div> <!-- End row -->
    
    <?php endwhile; ?>

<?php get_footer(); ?>