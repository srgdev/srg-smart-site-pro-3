<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>

    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width">
    
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    
    <link href='http://fonts.googleapis.com/css?family=Oswald:400,300,700|Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
    
    <?php wp_head(); ?>
    
</head>

<body <?php body_class('bgcolor-body txtcolor-body'); ?>>

<div id="navRow" class="row">
	<div class="rowInner">
	
    	<div id="logo"><a href="<?php bloginfo('url'); ?>"><img src="<?php echo get_theme_mod('srg_theme_logo') ? get_theme_mod('srg_theme_logo') : get_template_directory_uri().'/images/logo.png'; ?>"></a></div>
        
        <?php srg_nav_menu('header'); ?>
        
        <br class="clear">
        
  </div> <!-- End rowInner -->
</div> <!-- End row -->