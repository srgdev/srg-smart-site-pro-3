<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

<div id="bannerRow" class="row subBanner bgcolor-primary" style="background-image:url(<?php echo get_theme_mod('srg_theme_banner_image_top') ? get_theme_mod('srg_theme_banner_image_top') : get_template_directory_uri().'/images/bg-bannerrow.jpg';?>)">
	<div class="rowInner">
    	
    	<h1><?php printf( __( 'Search Results for: %s', 'srg' ), '' . get_search_query() . '' ); ?></h1>
	
    </div> <!-- End rowInner -->
</div> <!-- End row -->


<div id="contentRow" class="row">
	<div class="rowInner">
        
        <?php if ( have_posts() ) : ?>
        
        	<?php get_template_part('loop', 'index'); ?>
            
        <?php else : ?>
        
        <h2><?php _e( 'Nothing Found', 'twentyten' ); ?></h2>
					<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'srg' ); ?></p>
                    
                     <div class="sideItem searchItem">
            <form method="get" action="<?php bloginfo('url'); ?>">
                <input name="s" type="text" placeholder="search <?php echo $_GET['post_type']; ?>s" class="field searchPage">
                <input type="hidden" name="post_type" value="<?php echo $_GET['post_type']; ?>" />
                <input name="submit" type="submit" value="&#xf002;" class="submit">
                <br class="clear">
            </form>
        </div>
        
        <?php endif; ?>
    	
  	</div> <!-- End rowInner -->
</div> <!-- End row -->

<?php get_footer(); ?>
