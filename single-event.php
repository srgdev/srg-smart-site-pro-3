<?php
/**
 * The Template for displaying all single events.
 *
 * @package WordPress
 * @subpackage Starkers
 * @since Starkers 3.0
 */

get_header(); ?>

<div id="bannerRow" class="row subBanner bgcolor-primary" style="background-image:url(<?php echo get_theme_mod('srg_theme_banner_image_top') ? get_theme_mod('srg_theme_banner_image_top') : get_template_directory_uri().'/images/bg-bannerrow.jpg';?>)">
	<div class="rowInner">
    	
    	<h1><?php _e('Events', 'srg'); ?></h1>
	
    </div> <!-- End rowInner -->
</div> <!-- End row -->

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

    <div id="contentRow" class="row">
        <div class="rowInner <?php echo is_active_sidebar('blog-sidebar') ? 'hasSidebar' : ''; ?>">
        
                <?php if(is_active_sidebar('blog-sidebar')): ?>
                    
                        <?php get_sidebar('blog'); ?>
                    
                <?php endif; ?>
                
                <div id="content">
                
                	<h2><?php the_title(); ?></h2>
            
                    <h4> Event Date: <?php echo date('F j, Y', strtotime(get_field('start_date'))); ?>
					<?php echo get_field('end_date') && get_field('end_date') != get_field('start_date') ? ' - '.date('F j, Y', strtotime(get_field('end_date'))) : ''; ?>, 
					<?php echo get_field('times') ? get_field('times') : ''; ?>
                    </h4>

            
	                <?php get_template_part('article', 'content'); ?>
                
                </div>
            
        </div> <!-- End rowInner -->
    </div> <!-- End row -->
    
    <?php endwhile; ?>

<?php get_footer(); ?>