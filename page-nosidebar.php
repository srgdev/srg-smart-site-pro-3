<?php
/**
 * Template name: Page no sidebar
 *
 */

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div id="bannerRow" class="row subBanner bgcolor-primary" style="background-image:url(<?php echo get_theme_mod('srg_theme_banner_image_top') ? get_theme_mod('srg_theme_banner_image_top') : get_template_directory_uri().'/images/bg-bannerrow.jpg';?>)">
	<div class="rowInner">
    	
    	<h1><?php the_title(); ?></h1>
	
    </div> <!-- End rowInner -->
</div> <!-- End row -->


<div id="contentRow" class="row">
	<div class="rowInner">
                
            <div id="content">
            
            <?php the_content(); ?>
        
        </div> <!-- End content -->
    	
  	</div> <!-- End rowInner -->
</div> <!-- End row -->

<?php endwhile; ?>

<?php get_footer(); ?>