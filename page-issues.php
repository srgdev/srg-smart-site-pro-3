<?php
/**
 * Template name: issues
 *
 */

get_header(); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<div id="bannerRow" class="row subBanner bgcolor-primary" style="background-image:url(<?php echo get_theme_mod('srg_theme_banner_image_top') ? get_theme_mod('srg_theme_banner_image_top') : get_template_directory_uri().'/images/bg-bannerrow.jpg';?>)">
	<div class="rowInner">
    	
    	<h1><?php the_title(); ?></h1>
	
    </div> <!-- End rowInner -->
</div> <!-- End row -->


<div id="contentRow" class="row">
	<div class="rowInner <?php echo is_active_sidebar('blog-sidebar') ? 'hasSidebar' : ''; ?>">
    
    		<?php if(is_active_sidebar('blog-sidebar')): ?>
            	
                	<?php get_sidebar('blog'); ?>
                    
            <?php endif; ?>
                
            <div id="content" class="issues">
            
            <?php the_content(); ?>
            
            <?php if(have_rows('issues')): ?>
                <?php while(have_rows('issues')): the_row(); ?>
                    <div class="post">
                        <?php if(get_sub_field('image')): ?>
                            <div class="postHeader">
                                <div class="postImage"><img src="<?php the_sub_field('image'); ?>" /></div>
                             </div>
                        <?php endif; ?>
                        <div class="postContent">
                            <h3 class="txtcolor-primary"><?php the_sub_field('title'); ?></h3>
                            <?php the_sub_field('content'); ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            <?php endif; ?>
        
        </div> <!-- End content -->
    	
  	</div> <!-- End rowInner -->
</div> <!-- End row -->

<?php endwhile; ?>

<?php get_footer(); ?>