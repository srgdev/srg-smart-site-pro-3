<?php

/**
* SRG FirstClass Form Widget - Works closely with Createsend API to manage and generate lists for clients and add subscribers
*
* by: Calvin deClaisse-Walford
* v: 1.0
*
*/

class SRG_FirstClass_Widget extends WP_Widget {

	function __construct() {
		parent::__construct('SRG_FirstClass_Widget', 'SRG FirstClass Form Widget', array( 'description' => 'FirstClass form Widget for homepage')	);
	}
	
	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
        $firstclassOpts = get_option('srg-firstclass-settings');
        $thankspage = $firstclassOpts['pageId'];
		?>
        <?php if(is_front_page()): ?>   
            <div id="colSignup" class="column">
        <?php else: ?>
            <div class="sideItem signupItem">
        <?php endif; ?>
        <h2><?php echo $instance['title']; ?></h2>
        <?php if($firstclassOpts): ?>
        		<div class="spinner">
                  <div class="rect1 bgcolor-secondary"></div>
                  <div class="rect2 bgcolor-secondary"></div>
                  <div class="rect3 bgcolor-secondary"></div>
                  <div class="rect4 bgcolor-secondary"></div>
                  <div class="rect5 bgcolor-secondary"></div>
                </div>
                <form method="post" action="#" data-validate="parsley" id="signupform" parsley-validate="true">
                    <input required name="name" type="text" placeholder="your name" class="field" data-required="true">
                    <input required name="email" type="text" placeholder="email address" class="field" data-required="true" data-type="email">
                    <input required name="zip" type="text" placeholder="zip" class="field small" data-required="true">
                    <input name="submit" type="submit" value="<?php echo strtoupper($instance['text']); ?>" class="submit bgcolor-secondary">
                    <input type="hidden" name="action" id="action" value="firstclass_signup"  />
                    <input type="hidden" name="redirect" id="redirect" value="<?php echo get_permalink($thankspage); ?>"  />
                </form>
                 <br class="clear" />
        <?php else: ?>
                <div  class="noPosts">
                	<h1 class="noPostsMessage">Whoops!  You're FirstClass account settings need to be updated or there is an issue!</h1>
                </div>
        <?php endif; ?>
        </div>
        <?php
	}
			
	// Widget Backend 
	public function form( $instance ) {
		
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		} else {
			$title = 'Stay Informed';
		}
		
		if ( isset( $instance[ 'text' ] ) ) {
			$text = $instance[ 'text' ];
		} else {
			$text = 'Sign me up!';
		}
		
		// Widget admin form
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
        
        <label for="<?php echo $this->get_field_id( 'text' ); ?>"><?php _e( 'Submit button text' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'text' ); ?>" name="<?php echo $this->get_field_name( 'text' ); ?>" type="text" value="<?php echo esc_attr( $text ); ?>" />
		</p>
 
		<?php 


	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['text'] = ( ! empty( $new_instance['text'] ) ) ? strip_tags( $new_instance['text'] ) : '';
		return $instance;
		
	}
	
}

// Register and load the widget
function srg_firstclass_widget_load() {
	register_widget( 'SRG_FirstClass_Widget' );
}
add_action( 'widgets_init', 'srg_firstclass_widget_load' );

?>