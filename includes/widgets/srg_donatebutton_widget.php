<?php

/**
* SRG Donate Button Widget - Generates a simple donate button in a widget
*
* by: Calvin deClaisse-Walford
* v: 1.0
*
*/

class SRG_DonateButton_Widget extends WP_Widget {

	function __construct() {
		parent::__construct('SRG_DonateButton_Widget', 'SRG Donate Button Widget', array( 'description' => 'Simple Donate Button Widget')	);
	}
	
	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		?>
		<?php if(get_theme_mod('srg_theme_donate_link')): ?>
            <?php if(is_front_page()): ?>
            <div id="colDonate" class="column">
            	<h2><?php echo $instance['title']; ?></h2>
            	<a href="<?php echo get_theme_mod('srg_theme_donate_link'); ?>" class="btnDonate bgcolor-secondary killswitch"><h1><?php echo strtoupper($instance['text']); ?></h1></a>
            </div>
            <?php else: ?>
            <a href="<?php echo get_theme_mod('srg_theme_donate_link'); ?>" class="btnDonate bgcolor-secondary killswitch"><h1><?php echo strtoupper($instance['text']); ?></h1></a>
                <br class="clear" />
            <?php endif; ?>
        <?php endif; ?>
        <?php
	}
			
	// Widget Backend 
	public function form( $instance ) {
		
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		} else {
			$title = 'Donate Today';
		}
		
		if ( isset( $instance[ 'text' ] ) ) {
			$text = $instance[ 'text' ];
		} else {
			$text = 'Donate';
		}
		
		// Widget admin form
		?>
		
		<?php if(get_theme_mod('srg_theme_donate_link')): ?>
		
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
        
        <p>
		<label for="<?php echo $this->get_field_id( 'text' ); ?>"><?php _e( 'Button Text:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'text' ); ?>" name="<?php echo $this->get_field_name( 'text' ); ?>" type="text" value="<?php echo esc_attr( $text ); ?>" />
		</p>
		
		<p>Donate link: <?php echo get_theme_mod('srg_theme_donate_link'); ?></p>

        <?php else: ?>
            <p style="color:red;">Please add a donate url in the "Customize" options before adding a donate button.</p>
        <?php endif; ?>
        
		<?php 

	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['text'] = ( ! empty( $new_instance['text'] ) ) ? strip_tags( $new_instance['text'] ) : '';
		return $instance;
	}
	
}

// Register and load the widget
function srg_donatebutton_widget_load() {
	register_widget( 'SRG_DonateButton_Widget' );
}
add_action( 'widgets_init', 'srg_donatebutton_widget_load' );

?>