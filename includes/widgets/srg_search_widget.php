<?php

/**
* SRG Search Widget - Generate a search form for current post type
*
* by: Calvin deClaisse-Walford
* v: 1.0
*
*/

class SRG_Search_Widget extends WP_Widget {

	function __construct() {
		parent::__construct('SRG_Search_Widget', 'SRG Search Widget', array( 'description' => 'Search form Widget')	);
	}
	
	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {

		?>
        
        <?php if(!is_front_page()): ?>
        
        <div class="sideItem searchItem">
            <form method="get" action="<?php bloginfo('url'); ?>">
                <input name="s" type="text" placeholder="<?php echo $instance['title'].' '.get_post_type(); ?>s" class="field">
                <input type="hidden" name="post_type" value="<?php echo get_post_type(); ?>" />
                <input name="submit" type="submit" value="&#xf002;" class="submit">
                <br class="clear">
            </form>
        </div>
        
        <?php endif; ?>
        
        <?php
	}
			
	// Widget Backend 
	public function form( $instance ) {
		
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		} else {
			$title = 'Search';
		}
		
		// Widget admin form
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
        
		<?php 


	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
		
	}
	
}

// Register and load the widget
function srg_search_widget_load() {
	register_widget( 'SRG_Search_Widget' );
}
add_action( 'widgets_init', 'srg_search_widget_load' );

?>