<?php

/**
* SRG Facebook Widget - returnsfacebook likes iframe
*
* by: Calvin deClaisse-Walford
* v: 1.0
*
*/

class SRG_Facebook_Widget extends WP_Widget {

	function __construct() {
		parent::__construct('SRG_Facebook_Widget', 'SRG Facebook Widget', array( 'description' => 'Facebook Widget')	);
	}
	
	// Creating widget front-end
	// This is where the action happens
	public function widget( $args, $instance ) {
		
		$title = apply_filters( 'widget_title', $instance['title'] );
		$url = rawurlencode(get_theme_mod('srg_theme_fb'));
		
		?>
        
        <?php if(is_front_page()): ?>
        
       <div id="colFacebook" class="column">
       
       <?php else: ?>
       
       <div class="sideItem fbItem">
       
       <?php endif; ?>
       
        	<h2><?php echo $title; ?></h2>
        	<iframe src="https://www.facebook.com/plugins/likebox.php?href=<?php echo $url; ?>&amp;width&amp;height=190&amp;colorscheme=light&amp;show_faces=true&amp;header=false&amp;stream=false&amp;show_border=false&amp;appId=168922799959916" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:190px;" allowTransparency="true"></iframe>
        </div>
        
        <?php
	}
			
	// Widget Backend 
	public function form( $instance ) {
		
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		} else {
			$title = 'Stay Connected';
		}
		
		
		// Widget admin form
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>" />
		</p>
        
		<?php 
	}
		
	// Updating widget replacing old instances with new
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		return $instance;
	}

}

// Register and load the widget
function srg_facebook_widget_load() {
	register_widget( 'SRG_Facebook_Widget' );
}
add_action( 'widgets_init', 'srg_facebook_widget_load' );

?>