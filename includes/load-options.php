<?php
/**
 * Add some custom options to the customize panel
 *
 *@uses $wp_customize object for setting Theme Options panel sections
 *@uses add_setting()
 *@uses add_section()
 *@uses add_control()
 */
function srg_customize_register($wp_customize) {

    require_once(TEMPLATEPATH . '/includes/classes/WP_Customize_Control_Textarea.php');

    // Add settings from registered options
    $wp_customize->add_setting( 'srg_theme_logo', array('default' => false,'type' => 'theme_mod') );
    $wp_customize->add_setting( 'srg_theme_logo_footer', array('default' => false,'type' => 'theme_mod') );
    $wp_customize->add_setting( 'srg_theme_banner_image_top', array('default' => false,'type' => 'theme_mod') );
    $wp_customize->add_setting( 'srg_theme_primary_color', array('default' => '#225395','type' => 'theme_mod') );
    $wp_customize->add_setting( 'srg_theme_secondary_color', array('default' => '#ee283e','type' => 'theme_mod') );
    $wp_customize->add_setting( 'srg_theme_body_bg_color', array('default' => '#ffffff','type' => 'theme_mod') );
    $wp_customize->add_setting( 'srg_theme_text_primary_color', array('default' => '#ee283e','type' => 'theme_mod') );
    $wp_customize->add_setting( 'srg_theme_text_secondary_color', array('default' => '#ee283e','type' => 'theme_mod') );
    $wp_customize->add_setting( 'srg_theme_text_body_color', array('default' => '#666','type' => 'theme_mod') );
    $wp_customize->add_setting( 'srg_theme_link_primary_color', array('default' => '#ee283e','type' => 'theme_mod') );
    $wp_customize->add_setting( 'srg_theme_link_secondary_color', array('default' => '#ee283e','type' => 'theme_mod') );
    $wp_customize->add_setting( 'srg_theme_link_body_color', array('default' => '#666','type' => 'theme_mod') );
    $wp_customize->add_setting( 'srg_theme_address', array('default' => false));
    $wp_customize->add_setting( 'srg_theme_phone', array('default' => false));
    $wp_customize->add_setting( 'srg_theme_email', array('default' => false));
    $wp_customize->add_setting( 'srg_theme_paidfor', array('default' => false));
    $wp_customize->add_setting( 'srg_theme_fb', array('default' => false));
    $wp_customize->add_setting( 'srg_theme_tw', array('default' => false));
    $wp_customize->add_setting( 'srg_theme_donate_link', array('default' => false));
    $wp_customize->add_setting( 'srg_theme_numslides', array('default' => false));
    $wp_customize->add_setting( 'srg_theme_showslider', array('default' => false));
    $wp_customize->add_setting( 'srg_theme_showHomeNews', array('default' => false));
    $wp_customize->add_setting( 'srg_theme_showNumPosts', array('default' => false));
    $wp_customize->add_setting( 'srg_theme_homeNewsTitle', array('default' => false));
    $wp_customize->add_setting( 'srg_theme_showWidgets', array('default' => false));
    // Analytics
    $wp_customize->add_setting( 'srg_theme_analytics', array(	'default' => false,'type' => 'theme_mod') );

    // Add options sections
    $wp_customize->add_section( 'srg_header_options', array('title' => 'Header Options','priority' => 31) );
    $wp_customize->add_section( 'srg_banner_options', array('title' => 'Banner Options','priority' => 32) );
    $wp_customize->add_section( 'srg_homepage_options', array('title' => 'Home Page Options','priority' => 33) );
    $wp_customize->add_section( 'srg_social_options', array('title' => 'Social/General Options','priority' => 34) );
    $wp_customize->add_section( 'srg_color_options', array('title' => 'Color Options','priority' => 35) );
    $wp_customize->add_section( 'srg_text_options', array('title' => 'Text Options','priority' => 36) );
    $wp_customize->add_section( 'srg_link_options', array('title' => 'Link Options','priority' => 37) );
    $wp_customize->add_section( 'srg_footer_options', array('title' => 'Footer Options','priority' => 38) );

    // Add controls to our options
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'header_image', array('label'   => 'Header Image','section' => 'srg_header_options','settings'   => 'srg_theme_logo',) ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'footer_image', array('label'   => 'Footer Image','section' => 'srg_footer_options','settings'   => 'srg_theme_logo_footer', ) ) );
    $wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'banner_image_top', array('label'   => 'Slider Background Image','section' => 'srg_banner_options','settings'   => 'srg_theme_banner_image_top', ) ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'primary_color', array('label'   => 'Primary Color', 'section' => 'srg_color_options','settings'   => 'srg_theme_primary_color', ) ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'secondary_color', array('label'   =>  'Secondary Color', 'section' => 'srg_color_options','settings'   => 'srg_theme_secondary_color',) ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'body_background_color', array('label'   => 'Body Background Color','section' => 'srg_color_options','settings'   => 'srg_theme_body_bg_color',) ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'srg_text_primary_color', array('label'   => 'Primary Text Color','section' => 'srg_text_options','settings'   => 'srg_theme_text_primary_color',) ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'txt_secondary_color', array('label'   => 'Secondary Text Color','section' => 'srg_text_options','settings'   => 'srg_theme_text_secondary_color',) ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'txt_body_color', array('label'   => 'Body Text Color','section' => 'srg_text_options','settings'   => 'srg_theme_text_body_color',) ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'link_primary_color', array('label'   =>  'Primary Link Color','section' => 'srg_link_options','settings'   => 'srg_theme_link_primary_color',) ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'link_secondary_color', array('label'   => 'Secondary Link Color','section' => 'srg_link_options','settings'   => 'srg_theme_link_secondary_color',) ) );
    $wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'link_body_color', array('label'   => 'Body Link Color','section' => 'srg_link_options','settings'   => 'srg_theme_link_body_color',) ) );
    $wp_customize->add_control( 'srg_theme_address', array('label' => __( 'Address'), 'section' => 'srg_social_options'));
    $wp_customize->add_control( 'srg_theme_phone', array('label' => __( 'Phone'), 'section' => 'srg_social_options'));
    $wp_customize->add_control( 'srg_theme_email', array('label' => __( 'Email'), 'section' => 'srg_social_options'));
    $wp_customize->add_control( 'srg_theme_paidfor', array('label' => __( 'Paid for by... text'), 'section' => 'srg_footer_options'));
    $wp_customize->add_control( 'srg_theme_fb', array('label' => __( 'Facebook Page URL'), 'section' => 'srg_social_options'));
    $wp_customize->add_control( 'srg_theme_tw', array('label' => __( 'Twitter Username/Handle'), 'section' => 'srg_social_options'));
    $wp_customize->add_control( 'srg_theme_donate_link', array('label' => __( 'Donate URL'), 'section' => 'srg_social_options'));
    // Slider opts
    $wp_customize->add_control( 'srg_theme_numslides', array('label' => __( 'Number of slides' ),'section' => 'srg_banner_options',) );
    $wp_customize->add_control( new WP_Customize_Control($wp_customize,'showslider', array('label' => __( 'Show the slider?', 'srg' ),'section'=> 'srg_banner_options','settings' => 'srg_theme_showslider','type' => 'checkbox') ) );
    // News opts
    $wp_customize->add_control( 'srg_theme_homeNewsTitle', array('label' => __( 'News Section Title'), 'section' => 'srg_homepage_options'));
    $wp_customize->add_control( 'srg_theme_showNumPosts', array('label' => __( 'Number of rows' ),'section' => 'srg_homepage_options',) );
    $wp_customize->add_control( new WP_Customize_Control($wp_customize,'shownews', array('label' => __( 'Show news feed?', 'srg' ),'section'=> 'srg_homepage_options','settings' => 'srg_theme_showHomeNews','type' => 'checkbox') ) );
    // Widget opts
     $wp_customize->add_control( new WP_Customize_Control($wp_customize,'showwidgets', array('label' => __( 'Show Widget Area?', 'srg' ),'section'=> 'srg_homepage_options','settings' => 'srg_theme_showWidgets','type' => 'checkbox') ) );
    // Analytics
    $wp_customize->add_control( new WP_Customize_Control_Textarea( $wp_customize, 'srg_theme_analytics', array('label' => __( 'Google Analytics Code'), 'section' => 'srg_social_options', 'settings' => 'srg_theme_analytics')));
}

add_action( 'customize_register', 'srg_customize_register' );

/*----------------------------------------------------------------------------------------------------*/

/**
 * Check options that contain urls and ensure they are on the local domain
 */
function check_options_urls(){

    $current_url = get_bloginfo('url');

    $opts = array();
    $opts['srg_theme_logo'] = get_theme_mod('srg_theme_logo');
    $opts['srg_theme_logo_footer'] = get_theme_mod('srg_theme_logo_footer');
    $opts['srg_theme_banner_image_top'] = get_theme_mod('srg_theme_banner_image_top');

    // Loop
    foreach($opts as $name=>$opt){

        // If it has been set
        if($opt != ''){

            // Figure out what the old url was, so we can strip it out if it is different
            $old_url = substr($opt, 0, strpos($opt, '/wp-content')) . '<br>';

            // If they dont match
            if($old_url != $current_url){

                // Get the wp-content path
                $path = substr($opt, strpos($opt, '/wp-content'), strlen($opt));

                // Add current url to beginning
                $path = $current_url . $path;

                // Set the theme mod
                set_theme_mod($name, $path);
            }
        }
    }
}
add_action('init', 'check_options_urls');
