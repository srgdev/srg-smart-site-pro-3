<?php
/**
* Register Sidebars
* @uses register_sidebar()
*/
function srg_sidebars_init() {
        register_sidebar( array(
            'name' => __( 'Home Page Widgetbox', 'srg' ),
            'id' => 'home-box',
            'description' => __( 'The home page widgets area', 'srg' ),
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '',
            'after_title' => '',
        ) );
        register_sidebar( array(
            'name' => __( 'Main Sidebar', 'srg' ),
            'id' => 'blog-sidebar',
            'description' => __( 'The blog page and posts sidebar', 'srg' ),
            'before_widget' => '',
            'after_widget' => '',
            'before_title' => '>',
            'after_title' => '',
        ) );
}
// Register sidebars in init
add_action( 'widgets_init', 'srg_sidebars_init' );