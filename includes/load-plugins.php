<?php
/**
 * Load packaged plugins, define some custom settings and globals
 * @uses srg_custom_fields()
 * @hook after_setup_theme
 */
function srg_plugin_loader(){
    define( 'ACF_LITE', true );
    include(TEMPLATEPATH . '/includes/plugins/advanced-custom-fields/acf.php');
    include(TEMPLATEPATH . '/includes/plugins/acf-repeater/acf-repeater.php');
    include(TEMPLATEPATH . '/includes/plugins/add-custom-post-types-archive-to-nav-menus/cpt-in-navmenu.php');
    
    // Setup custom fields
    srg_custom_fields();
}
add_action( 'after_setup_theme', 'srg_plugin_loader' );

/*----------------------------------------------------------------------------------------------------*/

/**
* Register custom fields
*/
function srg_custom_fields(){
    if(function_exists("register_field_group"))
        {
            register_field_group(array (
                'id' => 'acf_events',
                'title' => 'Events',
                'fields' => array (
                    array (
                        'key' => 'field_531dc6feb5005',
                        'label' => 'Start Date',
                        'name' => 'start_date',
                        'type' => 'date_picker',
                        'instructions' => 'Choose event start date',
                        'required' => 1,
                        'date_format' => 'yymmdd',
                        'display_format' => 'DD, MM d, yy',
                        'first_day' => 0,
                    ),
                    array (
                        'key' => 'field_531dc758b5006',
                        'label' => 'End Date',
                        'name' => 'end_date',
                        'type' => 'date_picker',
                        'instructions' => 'End date of event',
                        'date_format' => 'yymmdd',
                        'display_format' => 'DD, MM d, yyyy',
                        'first_day' => 0,
                    ),
                    array (
                        'key' => 'field_531dc77cb5007',
                        'label' => 'Times',
                        'name' => 'times',
                        'type' => 'text',
                        'instructions' => 'Enter the times of the event here, e.g. 7:00pm - 10:00pm',
                        'default_value' => '',
                        'placeholder' => '6:00pm - 9:00pm',
                        'prepend' => '',
                        'append' => '',
                        'formatting' => 'html',
                        'maxlength' => '',
                    ),
                ),
                'location' => array (
                    array (
                        array (
                            'param' => 'post_type',
                            'operator' => '==',
                            'value' => 'event',
                            'order_no' => 0,
                            'group_no' => 0,
                        ),
                    ),
                ),
                'options' => array (
                    'position' => 'normal',
                    'layout' => 'no_box',
                    'hide_on_screen' => array (
                    ),
                ),
                'menu_order' => 0,
            ));
            
           register_field_group(array (
            'id' => 'acf_issues',
            'title' => 'Issues',
            'fields' => array (
                array (
                    'key' => 'field_533af74d745b3',
                    'label' => 'Issues',
                    'name' => 'issues',
                    'type' => 'repeater',
                    'instructions' => 'Add issues here.',
                    'sub_fields' => array (
                        array (
                            'key' => 'field_533af765745b4',
                            'label' => 'Image',
                            'name' => 'image',
                            'type' => 'image',
                            'instructions' => 'Upload/select image here.',
                            'column_width' => 20,
                            'save_format' => 'url',
                            'preview_size' => 'thumbnail',
                            'library' => 'all',
                        ),
                        array (
                            'key' => 'field_533af77d745b5',
                            'label' => 'Title',
                            'name' => 'title',
                            'type' => 'text',
                            'instructions' => 'Issue Title',
                            'required' => 1,
                            'column_width' => 20,
                            'default_value' => '',
                            'placeholder' => 'Title..',
                            'prepend' => '',
                            'append' => '',
                            'formatting' => 'html',
                            'maxlength' => '',
                        ),
                        array (
                            'key' => 'field_533af798745b6',
                            'label' => 'Content',
                            'name' => 'content',
                            'type' => 'wysiwyg',
                            'instructions' => 'Explain the issue',
                            'required' => 1,
                            'column_width' => 60,
                            'default_value' => '',
                            'toolbar' => 'basic',
                            'media_upload' => 'no',
                        ),
                    ),
                    'row_min' => '',
                    'row_limit' => '',
                    'layout' => 'table',
                    'button_label' => 'Add Row',
                ),
            ),
            'location' => array (
                array (
                    array (
                        'param' => 'page_template',
                        'operator' => '==',
                        'value' => 'page-issues.php',
                        'order_no' => 0,
                        'group_no' => 0,
                    ),
                ),
            ),
            'options' => array (
                'position' => 'normal',
                'layout' => 'no_box',
                'hide_on_screen' => array (
                ),
            ),
            'menu_order' => 0,
        ));
        };
    };