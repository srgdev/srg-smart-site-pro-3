<?php
/**
 * The template for displaying Archive pages.
 *
 */

get_header(); ?>

<div id="bannerRow" class="row subBanner bgcolor-primary" style="background-image:url(<?php echo get_theme_mod('srg_theme_banner_image_top') ? get_theme_mod('srg_theme_banner_image_top') : get_template_directory_uri().'/images/bg-bannerrow.jpg';?>)">
	<div class="rowInner">
    	
        <?php if ( have_posts() ) the_post(); ?>
        
    	<h1><?php if ( is_day() ) : ?>
					<?php printf( __( 'Daily Archives: %s', 'twentyten' ), get_the_date() ); ?>
				<?php elseif ( is_month() ) : ?>
                                <?php printf( __( 'Monthly Archives: %s', 'twentyten' ), get_the_date('F Y') ); ?>
                <?php elseif ( is_year() ) : ?>
                                <?php printf( __( 'Yearly Archives: %s', 'twentyten' ), get_the_date('Y') ); ?>
                <?php else : ?>
                                <?php _e( 'Blog Archives', 'twentyten' ); ?>
                <?php endif; ?></h1>
                
        <?php rewind_posts(); ?>
	
    </div> <!-- End rowInner -->
</div> <!-- End row -->


<div id="contentRow" class="row">
	<div class="rowInner <?php echo is_active_sidebar('blog-sidebar') ? 'hasSidebar' : ''; ?>">
    
    		<?php if(is_active_sidebar('blog-sidebar')): ?>
            	
                	<?php get_sidebar('blog'); ?>
                
            <?php endif; ?>
        
        	<?php get_template_part('loop', 'index'); ?>
    	
  	</div> <!-- End rowInner -->
</div> <!-- End row -->

<?php get_footer(); ?>